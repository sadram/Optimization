% lsd : proximal operator

n = 1001; % resolution
xmax = 10;
Xi = linspace(-xmax, xmax, n);

Q = [1,4/3,3/2,2,3,4]

nbq = size(Q,2);

chi = 1;

for i = 1:nbq
    q = Q(i);
    y = proximal_operator(Xi, q, chi);
    plot(Xi, y);
    hold on;
end

% plot proximal operator
figure(1)
legend('1', '4/3', '3/2', '2', '3', '4');
axis('equal');

% load image and plot noised image
figure(2)
title('original')
img = imread('florence.jpg');
% imshow(img)
img_noised = double(img) + 30.0 * randn(size(img));
title('Noisy')
image(uint8(img_noised));

% denoising with wavedec2


